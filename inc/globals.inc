<?php

function return_prices($array) {
	$returnArray = [
      'base_price' => 0,
      'discount' => 0,
      'tax' => 0,
    ];
	
	foreach($array as &$component) {
		if($component['name'] == 'base_price') {
			$returnArray['base_price'] = $component['price']['amount'];
		} elseif(strpos($component['name'], 'discount') !== false) {
			$returnArray['discount'] = $returnArray['discount'] + $component['price']['amount'];
		} elseif(strpos($component['name'], 'tax') !== false) {
			$returnArray['tax'] = $component['price']['amount'];
		} else {
			$returnArray[$component['name']] = $component['price']['amount'];
		}
	}
	
	return $returnArray;
}

function custom_orders_counter($ordertype, $site = 'us') {
	if($site == 'us') {
		switch($ordertype) {
			case 'RM': $counter_id = 3156;	break;
			case 'AD': $counter_id = 3157;	break;
			case 'SC': $counter_id = 3158; break;
			case 'CW': $counter_id = 3159; break;
			case 'RS': $counter_id = 3160; break;
			case 'PR': $counter_id = 3161; break;
			case 'SV': $counter_id = 3170; break;
		}
		$type = $ordertype;
	} elseif ($site == 'hk') {
		switch($ordertype) {
			case 'RM': $counter_id = 928; break;
			case 'AD': $counter_id = 929; break;
			case 'SC': $counter_id = 930; break;
			case 'CW': $counter_id = 931; break;
			case 'RS': $counter_id = 932; break;
			case 'PR': $counter_id = 2172; break;
			case 'SV': $counter_id = 2991; break;
		}
		$type = $ordertype;
	} elseif ($site == 'de') {
		switch($ordertype) {
			case '2': $counter_id = 928; break;
			case '14': $counter_id = 929; break;
			case '3': $counter_id = 929; break;
			case '13': $counter_id = 930; break;
			case '4': $counter_id = 930; break;
			case '5': $counter_id = 931; break;
			case '6': $counter_id = 932; break;
			case '7': $counter_id = 933; break;
			case '8': $counter_id = 934; break;
			case '9': $counter_id = 935; break;
			case '12': $counter_id = 2172; break;
			case '10': $counter_id = 2172; break;
			case '11': $counter_id = 2310; break;
			case 'SV': $counter_id = 2484; break;
		}
		switch($ordertype) {
			case '2': $type = 'RM'; break;
			case '14': $type = 'AD'; break;
			case '3': $type = 'AD'; break;
			case '13': $type = 'SC'; break;
			case '4': $type = 'SC'; break;
			case '5': $type = 'CW'; break;
			case '6': $type = 'RS'; break;
			case '7': $type = 'SS'; break;
			case '8': $type = 'MS'; break;
			case '9': $type = 'ES'; break;
			case '12': $type = 'PR'; break;
			case '10': $type = 'PR'; break;
			case '11': $type = 'GS'; break;
			case 'SV': $type = 'SV'; break;
		}
	}  elseif ($site == 'uk') {
		switch($ordertype) {
			case '2': $counter_id = 928; break;
			case '3': $counter_id = 929; break;
			case '4': $counter_id = 930; break;
			case '5': $counter_id = 931; break;
			case '6': $counter_id = 932; break;
			case '7': $counter_id = 933; break;
			case '8': $counter_id = 934; break;
			case '9': $counter_id = 935; break;
			case '10': $counter_id = 2798; break;
			case 'SV': $counter_id = 3218; break;
		}
		switch($ordertype) {
			case '2': $type = 'RM'; break;
			case '3': $type = 'AD'; break;
			case '4': $type = 'SC'; break;
			case '5': $type = 'CW'; break;
			case '6': $type = 'RS'; break;
			case '7': $type = 'SS'; break;
			case '8': $type = 'MS'; break;
			case '9': $type = 'ES'; break;
			case '10': $type = 'PR'; break;
			case 'SV': $type = 'SV'; break;
		}
	}
	
	return array(
		'prefix' => $type,
		'counter_id' => $counter_id,
	);
}

function dynamics_failure($type, $order, $e, $xml) {
	global $user;
	$original_user = $user;
	$old_state = drupal_save_session();
	drupal_save_session(FALSE);
	$user = user_load(1);

	$errorNote = $e->getMessage();
	$order_xml = 'private://' . $type .'_order_xml/' . strtoupper($type) . '_XML_' . $order->order_number . '_' . date('YmdHis'). '.xml';
	file_unmanaged_save_data($xml, $order_xml);

	$user = $original_user;
	drupal_save_session($old_state);
	
	return array(
		'error' => $errorNote,
		'order_xml' => $order_xml,
	);
}

?>