<?php

function cambridge_invoicing_rules_condition_info() {
	return array(
		'failed_invoice' => array(
			'label' => t('Dynamics Check'),
			'group' => t('CA Dynamics HK'),
			'arguments' => array(
				'purchase_code' => array(
					'type' => 'text',
					'label' => t('Purchase Order response'),
					'description' => t('Error from Purchase Order'),
				),
				'applc_code' => array(
					'type' => 'text',
					'label' => t('APPLC response'),
					'description' => t('Error from APPLC Invoice'),
				),
				'caa_code' => array(
					'type' => 'text',
					'label' => t('CAA response'),
					'description' => t('Error from CAA Invoice'),
				),
			),
			'module' => 'cambridge_invoicing',
		),
	);
}

?>