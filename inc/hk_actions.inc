<?php

function hk_send_inventory_trx($dynamics_server, $order_id) {
	$order_wrapp = entity_metadata_wrapper('commerce_order', commerce_order_load($order_id));
	
	// Determining ORDER type
	switch($order_wrapp->field_order_type->value()) {
        case 'RM': $type = '71000-900-10'; break;
        case 'AD': $type = '80040-900-10'; break;
        case 'SC': $type = '80080-900-10'; break;
        case 'CW': $type = '80060-900-10'; break;
        case 'RS': $type = '80130-900-10'; break;
        case 'PR': $type = '80060-900-10'; break;
		case 'SV': $type = '80120-900-10'; break;
		case NULL: $type = NULL; break;
    }
	
	$server = ($dynamics_server == 'live') ? 'APPLC' : 'TRG01';
	
	//creating eConnect XML
	$eConnect = new SimpleXMLElement('<eConnect xmlns:dt="urn:schemas-microsoft-com:datatypes"></eConnect>');
	$IVInventoryTransactionType = $eConnect->addChild('IVInventoryTransactionType');
	
	//creating taSopLineIvcInsert_Items for line products
	$Items = $IVInventoryTransactionType->addChild('taIVTransactionLineInsert_Items');
	$HeaderInsert = $IVInventoryTransactionType->addChild('taIVTransactionHeaderInsert');

	//Line Item level data
	foreach($order_wrapp->commerce_line_items as $line) {
		if($line->type->value() == 'product') {
			//Query to get all Product Component nodes which has this item
			
			/* soap XML call parameters */
			$soap_params = [
				'MessageID'	=> 'Item',
				'DOCTYPE' => 'vwth_EC_Item_Comp_HK',
				'WHERECLAUSE' => "ITEMNMBR = '" . $line->commerce_product->sku->value() . "'",
			];

			/* converting $eConnect object to XML format || using soap_xml function */
			$sDoc = soap_xml($soap_params);

			/* call soap_call function from global.inc */
			$data = soap_trx_call($sDoc, $server);

			$components = [];
			if(!empty($data['eConnect'][0])) { foreach($data['eConnect'] as $component_raw) { $components[] = $component_raw[$soap_params['DOCTYPE']]; } }
			else { $components[] = $data['eConnect'][$soap_params['DOCTYPE']]; }
				
			foreach($components as &$component) {
				//create product components...
				$lineInsert = $Items->addChild('taIVTransactionLineInsert');
				$lineInsert->addChild('IVDOCNBR', $order_wrapp->order_number->value());
				$lineInsert->addChild('IVDOCTYP', '1');
				$lineInsert->addChild('ITEMNMBR', $component['CMPTITNM']);
				$lineInsert->addChild('TRXQTY', intval($line->quantity->value()) * intval($component['CMPITQTY']) * -1);
				$lineInsert->addChild('TRXLOCTN', 'FE-HK CAA');
				$lineInsert->addChild('InventoryAccountOffSet', $type);
			}
		}
	}
	
	//Adding Header level data
	$HeaderInsert->addChild('BACHNUMB', 'HK' . date('dmY', $order_wrapp->field_invoice_date->value()));
	$HeaderInsert->addChild('IVDOCNBR', $order_wrapp->order_number->value());
	$HeaderInsert->addChild('IVDOCTYP', '1');
	$HeaderInsert->addChild('DOCDATE', date('Y-m-d', $order_wrapp->field_invoice_date->value()));
	$HeaderInsert->addChild('TRXQTYTL', count($Items->children()));
	
	$params = array(
        'sConn' => 'Data Source=Audio25;Initial Catalog=' . $server .';Persist Security Info=False;Integrated Security=SSPI;',
        'sXML' => $eConnect->asXml(),
    );
	
	try {
        $client = new SoapClient('http://econnect.cambridgeaudio.com/msgp2013adapter/msgp2013adapter.asmx?wsdl');
        $resp = $client->CreateEntity($params);
    } catch (Exception $e) {
		$int_trx = dynamics_failure('inventory', $order, $e, $eConnect->asXml());
    }
	
	$file_uri = 'public://trx_xml/TRX_' . $order_wrapp->order_number->value() . '_' . date('YmdHis'). '.xml';
	file_save_data($eConnect->asXml(), $file_uri, FILE_EXISTS_RENAME);
	
		/* ///// PERFORM DOUBLE TEST CHECK ///// */
	
	$soap_params_test = [
		'MessageID'	=> 'Item',
		'DOCTYPE' =>'vwth_InventoryTransactions',
		'WHERECLAUSE' => "IVDOCNBR = '" . $order_wrapp->order_number->value() . "'",
	];

	/* converting $eConnect object to XML format || using soap_xml function */
	$test_sDoc = soap_xml($soap_params_test);

	/* call soap_call function from global.inc */
	$data_test = soap_trx_call($test_sDoc, $server, $order_wrapp);
	
	$dynamics_data = [];
	if(!empty($data_test['eConnect'][0])) {
		foreach($data_test['eConnect'] as $test_component_raw) {
			$dynamics_data[] = $test_component_raw[$soap_params_test['DOCTYPE']];
		}
	}
	else {
		$dynamics_data[] = $data_test['eConnect'][$soap_params_test['DOCTYPE']];
	}
	
	$xmlCorrect = TRUE;
	foreach($Items as $item) {
		foreach($dynamics_data as $d_data) {
			if(in_array((string)$item->ITEMNMBR, $d_data)) {
				if((intval($d_data['TRXQTY'])) != (float)$item->TRXQTY) {
					$xmlCorrect = FALSE;
				}
			} else {
				$xmlCorrect = FALSE;
			}
		}
	}
	
	if($xmlCorrect == FALSE) {
		/* Change this to your own default 'from' email address. */
		$from = variable_get('site_name', 'Default site name') . ' ' . strtoupper($country) . '<webadmin@cambridgeaudio.com>';
		$to = 'tim.huelin@cambridgeaudio.com';

		$body  = 'Hi,<br><br>';
		$body .= 'Error occured while checking TRX in dynamics for order <b>' . $order_wrapp->order_number->value() . '</b>:<br><br>';
		$body .= 'Information retrieved from Dynamics:<pre>' . print_r($dynamics_data, true) . '</pre>';
		$body .= 'XML sent:<pre>' . print_r($eConnect, true) . '</pre>';
		$body .= variable_get('site_name', 'Default site name') . ' ' . strtoupper($country);

		$cc = 'tim.huelin@cambridgeaudio.com, povilas.uogintas@cambridgeaudio.com';

		$params = [
			'context' => [
				'subject' => 'Error occured while checking TRX in dynamics for order ' . $order_wrapp->order_number->value(),
				'body' => $body,
			],
			'cc' => $cc,
			'bcc' => NULL,
			'reply-to' => $from,
			'plaintext' => '',
		];

		drupal_mail('mimemail', 'trx_component_check_error', $to, NULL, $params, $from);
		
		commerce_order_status_update($order_wrapp->value(), 'dynamics_fail');
	}
	
	/* ///// PERFORM DOUBLE TEST CHECK END ///// */

	return [
        'int_trx_soap_error' => !empty($int_trx['error']) ? $int_trx['error'] : NULL,
		'int_trx_xml' => !empty($int_trx['order_xml']) ? $int_trx['order_xml'] : NULL,
    ];
}

function hk_send_purchase_order($dynamics_server, $shop, $order_id) {
	$order = commerce_order_load($order_id);

	$CUSTNMBR = 'AP001';
	$locncode = 'FE-HK';
	$poNumber = 'PO' . $order->order_number;
	//creating eConnect XML
	$eConnect = new SimpleXMLElement('<eConnect xmlns:dt="urn:schemas-microsoft-com:datatypes"></eConnect>');
	$POPTransactionType = $eConnect->addChild('POPTransactionType');
	
	//creating taSopLineIvcInsert_Items for line products
	$Items = $POPTransactionType->addChild('taPoLine_Items');
	$HeaderInsert = $POPTransactionType->addChild('taPoHdr');
	
	//Adding Header level data
	$HeaderInsert->addChild('POTYPE', '1');
	$HeaderInsert->addChild('PONUMBER', $poNumber);
	$HeaderInsert->addChild('VENDORID', $CUSTNMBR);
	$HeaderInsert->addChild('DOCDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
	$HeaderInsert->addChild('BUYERID', 'A GROSSI');
	
	//Line Item level data
	foreach($order->commerce_line_items['und'] as &$line) {
		$line_item = commerce_line_item_load($line['line_item_id']);
		if($line_item->type == 'product') {
			$product = commerce_product_load($line_item->commerce_product['und'][0]['product_id']);

			// taSopLineIvcInsert
			$lineInsert = $Items->addChild('taPoLine');
			$lineInsert->addChild('POTYPE', '1');
			$lineInsert->addChild('PONUMBER', $poNumber);
			$lineInsert->addChild('VENDORID', $CUSTNMBR);
			$lineInsert->addChild('LOCNCODE', $locncode);
			$lineInsert->addChild('DOCDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
			$lineInsert->addChild('ITEMNMBR', $product->sku);
			$lineInsert->addChild('QUANTITY', round($line_item->quantity));
			$lineInsert->addChild('FREEONBOARD', '1');
			$lineInsert->addChild('REQDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
			$lineInsert->addChild('RELEASEBYDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
			$lineInsert->addChild('PRMDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
			$lineInsert->addChild('PRMSHPDTE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
		}
	}
	
	$server = ($dynamics_server == 'live') ? 'CAA' : 'TRG02';
	
	$params = array(
        'sConn' => 'Data Source=Audio25;Initial Catalog=' . $server .';Persist Security Info=False;Integrated Security=SSPI;',
        'sXML' => $eConnect->asXml(),
    );
	
	try {
        $client = new SoapClient('http://econnect.cambridgeaudio.com/msgp2013adapter/msgp2013adapter.asmx?wsdl');
        $resp = $client->CreateEntity($params);
    } catch (Exception $e) {
		$purchase = dynamics_failure('purchase', $order, $e, $eConnect->asXml());
    }

	return array(
        'purchase_soap_error' => !empty($purchase['error']) ? $purchase['error'] : NULL,
		'purchase_xml' => !empty($purchase['order_xml']) ? $purchase['order_xml'] : NULL,
    );
}

function hk_send_applc_invoice($dynamics_server, $shop, $order_type, $order_id) {
	$order = commerce_order_load($order_id);

	$CUSTNMBR = 'CA001';
	$locncode = 'FE-HK CAA';
		
	//creating eConnect XML
	$eConnect = new SimpleXMLElement('<eConnect xmlns:dt="urn:schemas-microsoft-com:datatypes"></eConnect>');
	$SOPTransactionType = $eConnect->addChild('SOPTransactionType');
	
	//creating taSopLineIvcInsert_Items for line products
	$Items = $SOPTransactionType->addChild('taSopLineIvcInsert_Items');
	$HeaderInsert = $SOPTransactionType->addChild('taSopHdrIvcInsert');
	$poNumber = ($order_type == 'custom') ? $order->order_number : 'PO' . $order->order_number;
	
	//Adding Header level data
	$HeaderInsert->addChild('SOPTYPE', '3');
	$HeaderInsert->addChild('DOCID', 'INVOICE');
	$HeaderInsert->addChild('SOPNUMBE', $order->order_number);
	$HeaderInsert->addChild('LOCNCODE', $locncode);
	$HeaderInsert->addChild('DOCDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
	$HeaderInsert->addChild('CUSTNMBR', $CUSTNMBR);
	$HeaderInsert->addChild('CSTPONBR', $poNumber);
	$HeaderInsert->addChild('USER2ENT', 'sa');
	$HeaderInsert->addChild('BACHNUMB', ($shop == 'b2c') ? 'B2C HK' : 'B2B HK');
	$HeaderInsert->addChild('PRBTADCD', 'MAIN');
	$HeaderInsert->addChild('USINGHEADERLEVELTAXES', '0');
	$HeaderInsert->addChild('CREATETAXES');
	$HeaderInsert->addChild('DEFTAXSCHDS', '0');
	$HeaderInsert->addChild('DEFPRICING', '1');
	$HeaderInsert->addChild('CURNCYID', $order->commerce_order_total['und'][0]['currency_code']);
	$HeaderInsert->addChild('FREIGTBLE', '1');
	$HeaderInsert->addChild('MISCTBLE', '0');
	
	//Line Item level data
	foreach($order->commerce_line_items['und'] as &$line) {
		$line_item = commerce_line_item_load($line['line_item_id']);
		if($line_item->type == 'product') {
			$product = commerce_product_load($line_item->commerce_product['und'][0]['product_id']);

			// taSopLineIvcInsert
			$lineInsert = $Items->addChild('taSopLineIvcInsert');
			$lineInsert->addChild('SOPTYPE', '3');
			$lineInsert->addChild('SOPNUMBE', $order->order_number);
			$lineInsert->addChild('CUSTNMBR', $CUSTNMBR);
			$lineInsert->addChild('DOCDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
			$lineInsert->addChild('LOCNCODE', $locncode);
			$lineInsert->addChild('ITEMNMBR', $product->sku);
			$lineInsert->addChild('QUANTITY', round($line_item->quantity));
			$lineInsert->addChild('LNITMSEQ', $line_item->line_item_id);
			$lineInsert->addChild('DEFPRICING', '1');
			$lineInsert->addChild('DEFEXTPRICE', '1');
			$lineInsert->addChild('DOCID', 'INVOICE');
			$lineInsert->addChild('CURNCYID', $order->commerce_order_total['und'][0]['currency_code']);
			$lineInsert->addChild('QTYSHRTOPT', 2);
		}
	}
	
	$server = ($dynamics_server == 'live') ? 'APPLC' : 'TRG01';
	
	$params = array(
        'sConn' => 'Data Source=Audio25;Initial Catalog=' . $server .';Persist Security Info=False;Integrated Security=SSPI;',
        'sXML' => $eConnect->asXml(),
    );
	
	try {
        $client = new SoapClient('http://econnect.cambridgeaudio.com/msgp2013adapter/msgp2013adapter.asmx?wsdl');
        $resp = $client->CreateEntity($params);
    } catch (Exception $e) {
		$applc = dynamics_failure('applc', $order, $e, $eConnect->asXml());
    }
	
	return array(
        'applc_soap_error' => !empty($applc['error']) ? $applc['error'] : NULL,
		'applc_xml' => !empty($applc['order_xml']) ? $applc['order_xml'] : NULL,
    );
}

function hk_send_caa_invoice($dynamics_server, $shop, $order_id) {
	$order = commerce_order_load($order_id);
	$user = user_load($order->uid);
	$shipment = commerce_customer_profile_load($order->commerce_customer_shipping['und'][0]['profile_id']);

	// user type
	if($shop == 'b2c') {
		if(in_array('partner', $user->roles)) {	$CUSTNMBR = 'EC202'; }
		elseif(in_array('colleague', $user->roles)) { $CUSTNMBR = $user->field_colleague_id['und'][0]['value']; }
		else { $CUSTNMBR = 'EC201';	}
	} else {
		$CUSTNMBR = $user->name;
	}
	
	$cstponbr = $shipment->commerce_customer_address['und'][0]['name_line'];
	
	$locncode = 'FE-HK';
		
	//creating eConnect XML
	$eConnect = new SimpleXMLElement('<eConnect xmlns:dt="urn:schemas-microsoft-com:datatypes"></eConnect>');
	$SOPTransactionType = $eConnect->addChild('SOPTransactionType');
	
	//creating taSopLineIvcInsert_Items for line products
	$Items = $SOPTransactionType->addChild('taSopLineIvcInsert_Items');
	if($shop == 'b2c') {
		$PaymentInsert = $SOPTransactionType->addChild('taCreateSopPaymentInsertRecord_Items')->addChild('taCreateSopPaymentInsertRecord');
	}
	$HeaderInsert = $SOPTransactionType->addChild('taSopHdrIvcInsert');
	
	//getting Order Total prices
	$orderPrices = return_prices($order->commerce_order_total['und'][0]['data']['components']);
	
	//Adding Header level data
	$HeaderInsert->addChild('SOPTYPE', '3');
	$HeaderInsert->addChild('DOCID', 'CAA INVOICE');
	$HeaderInsert->addChild('SOPNUMBE', $order->order_number);
	$HeaderInsert->addChild('LOCNCODE', $locncode);
	$HeaderInsert->addChild('DOCDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
	$HeaderInsert->addChild('CUSTNMBR', $CUSTNMBR);
	$HeaderInsert->addChild('CSTPONBR', $cstponbr);
	$HeaderInsert->addChild('USER2ENT', 'sa');
	$HeaderInsert->addChild('BACHNUMB', ($shop == 'b2c') ? 'B2C' : 'B2B');
	$HeaderInsert->addChild('PRBTADCD', 'MAIN');
	$HeaderInsert->addChild('FRTTXAMT', '0.00');
	$HeaderInsert->addChild('MSCTXAMT', '0.00');
	$HeaderInsert->addChild('USINGHEADERLEVELTAXES', '0');
	$HeaderInsert->addChild('CREATETAXES', '0');
	$HeaderInsert->addChild('DEFTAXSCHDS', '0');
	$HeaderInsert->addChild('CURNCYID', $order->commerce_order_total['und'][0]['currency_code']);
	$HeaderInsert->addChild('FREIGTBLE', '1');
	$HeaderInsert->addChild('MISCTBLE', '0');
	$HeaderInsert->addChild('TRDISAMT', !empty($orderPrices['discount']) ? number_format($orderPrices['discount']/-100, 2, '.', '') : NULL);
	$HeaderInsert->addChild('FREIGHT', '0.00');
	$HeaderInsert->addChild('SUBTOTAL', number_format($orderPrices['base_price']/100, 2, '.', ''));
	$HeaderInsert->addChild('DOCAMNT', number_format($order->commerce_order_total['und'][0]['amount']/100, 2, '.', ''));
	$HeaderInsert->addChild('PYMTRCVD', ($shop == 'b2c') ? number_format($order->commerce_order_total['und'][0]['amount']/100, 2, '.', '') : NULL);
	$HeaderInsert->addChild('TAXAMNT');
	$HeaderInsert->addChild('TAXSCHID');
	
	
	
	if($shop == 'b2c') {
		//adding Payment information
		$payment = commerce_payment_transaction_load_multiple(array(), array('order_id' =>  $order->order_id));
		foreach($payment as &$pay) {
			if($pay->payment_method =='braintree_dropin') {
				foreach($pay->payload as &$payload) {
					if($pay->status == 'success') {
						if($pay->message == 'Processor response: Approved<br />Credit card type: <br />Credit card last 4: ') {
							$chekbid = 'PAYPAL HKD';
						} else {
							$chekbid = 'BRAINTREE HKD';
						}
						$payAmmount = $pay->amount;
						$payCreated = $pay->created;
					}
				}
			}
		}
		
		$PaymentInsert->addChild('SOPTYPE', '3');
		$PaymentInsert->addChild('SOPNUMBE', $order->order_number);
		$PaymentInsert->addChild('CUSTNMBR', $CUSTNMBR);
		$PaymentInsert->addChild('DOCDATE', date('Y-m-d', $payCreated));
		$PaymentInsert->addChild('DOCAMNT', number_format($payAmmount/100, 2, '.', ''));
		$PaymentInsert->addChild('CHEKBKID', $chekbid);
		$PaymentInsert->addChild('PYMTTYPE', '4');
	}
	
	
	
	//Line Item level data
	foreach($order->commerce_line_items['und'] as &$line) {
		$line_item = commerce_line_item_load($line['line_item_id']);
		if($line_item->type == 'product') {
			$product = commerce_product_load($line_item->commerce_product['und'][0]['product_id']);
		
			$unitPrices = return_prices($line_item->commerce_unit_price['und'][0]['data']['components']);
			$totalPrices = $unitPrices['base_price'] * $line_item->quantity;

			// taSopLineIvcInsert
			$lineInsert = $Items->addChild('taSopLineIvcInsert');
			$lineInsert->addChild('SOPTYPE', '3');
			$lineInsert->addChild('SOPNUMBE', $order->order_number);
			$lineInsert->addChild('CUSTNMBR', $CUSTNMBR);
			$lineInsert->addChild('DOCDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
			$lineInsert->addChild('LOCNCODE', $locncode);
			$lineInsert->addChild('ITEMNMBR', $product->sku);
			$lineInsert->addChild('QUANTITY', round($line_item->quantity));
			$lineInsert->addChild('UNITPRCE', number_format($unitPrices['base_price']/100, 2, '.', ''));
			$lineInsert->addChild('XTNDPRCE', number_format($totalPrices/100, 2, '.', ''));
			$lineInsert->addChild('TAXAMNT');
			$lineInsert->addChild('LNITMSEQ', $line_item->line_item_id);
			$lineInsert->addChild('DOCID', 'CAA INVOICE');
			$lineInsert->addChild('CURNCYID', $order->commerce_order_total['und'][0]['currency_code']);
			$lineInsert->addChild('QTYSHRTOPT', 2);
		}
	}
	
	$server = ($dynamics_server == 'live') ? 'CAA' : 'TRG02';
	
	$params = array(
        'sConn' => 'Data Source=Audio25;Initial Catalog=' . $server .';Persist Security Info=False;Integrated Security=SSPI;',
        'sXML' => $eConnect->asXml(),
    );
	
	try {
        $client = new SoapClient('http://econnect.cambridgeaudio.com/msgp2013adapter/msgp2013adapter.asmx?wsdl');
        $resp = $client->CreateEntity($params);
		
    } catch (Exception $e) {
		$caa = dynamics_failure('caa', $order, $e, $eConnect->asXml());
    }
	
	return array(
        'caa_soap_error' => !empty($caa['error']) ? $caa['error'] : NULL,
		'caa_xml' => !empty($caa['order_xml']) ? $caa['order_xml'] : NULL,
    );
}

?>