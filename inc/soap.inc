<?php

/*
Name: soap_trx_call
Description: wrapper for general soap calls
 - $sDoc - the XML document which will be send to server
returns: array of data
*/
function soap_trx_call($sDoc, $server, $order_wrapp) {
	$sConn = 'Data Source=Audio25;Initial Catalog=' . $server .';Persist Security Info=False;Integrated Security=SSPI;';
	
	try	{
		/* soap call parameters */
		$params = [
			'sConn' => $sConn,
			'sDoc' => $sDoc
		];
		
		/* make soap call */
		$url = 'http://econnect.cambridgeaudio.com/msgp2013adapter/msgp2013adapter.asmx?wsdl';
		$client = new SoapClient($url);
		$resp = $client->GetEntity($params);
		
	} catch (SoapFault $e) {
		/* Change this to your own default 'from' email address. */
		$from = variable_get('site_name', 'Default site name') . '<webadmin@cambridgeaudio.com>';
		$to = 'tim.huelin@cambridgeaudio.com';

		$body  = 'Hi,<br><br>';
		$body .= 'Getting TRX components encountered errors on TRX ' . $order_wrapp->order_number->value() . ':<br><br>';
		$body .= 'SoapFault Exception: <pre>' . $e->getMessage() . '</pre>';
		$body .= 'sDoc sent:<pre>' . print_r($sDoc) . '</pre>';
		$body .= 'sConn sent: ' . $sConn . '<br><br>';
		$body .= variable_get('site_name', 'Default site name');

		$cc = 'tim.huelin@cambridgeaudio.com, povilas.uogintas@cambridgeaudio.com';

		$params = [
			'context' => [
				'subject' => 'Getting TRX ' . $order_wrapp->order_number->value() . ' components encountered errors',
				'body' => $body,
			],
			'cc' => $cc,
			'bcc' => NULL,
			'reply-to' => $from,
			'plaintext' => '',
		];

		drupal_mail('mimemail', 'trx_component_check_error', $to, NULL, $params, $from);
		
		commerce_order_status_update($order_wrapp->value(), 'dynamics_fail');
	}

	/* parse xml response to array */
	$xml = simplexml_load_string($resp->GetEntityResult, "SimpleXMLElement", LIBXML_NOCDATA);
	$json = json_encode($xml);
	$data = json_decode($json, true);
	
	return $data;
}

/*
Name: soap_xml
Description: wrapper for creating XML requests
 - $data - array the parameters
	- MessageID
	- DOCTYPE
	- (optional) WHERECLAUSE
returns: xml request
*/
function soap_xml($data) {
	/* creating eConnect XML */
	$eConnect = new SimpleXMLElement('<eConnect xmlns:dt="urn:schemas-microsoft-com:datatypes"></eConnect>');
	$RQeConnectOutType = $eConnect->addChild('RQeConnectOutType');

	/* creating eConnectProcessInfo header */
	$eConnectProcessInfo = $RQeConnectOutType->addChild('eConnectProcessInfo');
	$eConnectProcessInfo->addChild('Outgoing', 'TRUE');
	$eConnectProcessInfo->addChild('MessageID', $data['MessageID']);

	/* creating eConnectOut data */
	$eConnectOut = $RQeConnectOutType->addChild('eConnectOut');
	$eConnectOut->addChild('DOCTYPE', $data['DOCTYPE']);
	$eConnectOut->addChild('OUTPUTTYPE', '1');
	$eConnectOut->addChild('FORLOAD', '0');
	$eConnectOut->addChild('FORLIST', '1');
	$eConnectOut->addChild('ACTION', '0');
	$eConnectOut->addChild('ROWCOUNT', '0');
	$eConnectOut->addChild('REMOVE', '0');
	if(!empty($data['WHERECLAUSE'])) { $eConnectOut->addChild('WHERECLAUSE', $data['WHERECLAUSE']); }
	
	return $eConnect->asXml();
}

?>