<?php

function send_inventory_trx($dynamics_server, $country, $order_id) {
	$order_wrapp = entity_metadata_wrapper('commerce_order', commerce_order_load($order_id));
	
	if($country == 'de' || $country == 'uk') {
		$server = ($dynamics_server == 'live') ? 'APPLC' : 'TRG01';
	} elseif($country == 'us') {
		$server = ($dynamics_server == 'live') ? 'APLLC' : 'TRG02';
	}
	
	// Determining ORDER type
	$orderType = $order_wrapp->field_order_type->value();
	
	if($country == 'de') {
		switch($orderType) {
			case "2": $type = "71000-900-10"; break;
			case "14": $type = "80040-801-00"; break;
			case "3": $type = "80040-900-10"; break;
			case "13": $type = "80080-703-72"; break;
			case "4": $type = "80080-900-10"; break;
			case "5": $type = "80060-801-00"; break;
			case "6": $type = "80130-900-10"; break;
			case "7": $type = "94020-130-00"; break;
			case "8": $type = "94020-110-00"; break;
			case "9": $type = "94020-060-00"; break;
			case "11": $type = "94020-801-00"; break;
			case "12": $type = "80060-801-00"; break;
			case "10": $type = "80060-900-10"; break;
			case "SV": $type = "80120-900-10"; break;
			case NULL: $type = NULL; break;
		}
	}
	elseif($country == 'uk') {
		switch($orderType) {
			case "2": $type = "71000-900-10"; break;
			case "3": $type = "80040-900-10"; break;
			case "4": $type = "80080-900-10"; break;
			case "5": $type = "80060-900-10"; break;
			case "6": $type = "80130-900-10"; break;
			case "7": $type = "94020-130-00"; break;
			case "8": $type = "94020-110-00"; break;
			case "9": $type = "94020-060-00"; break;
			case "10": $type = "80060-900-10"; break;
			case "SV": $type = "80120-900-10"; break;
			case NULL: $type = NULL; break;
		}
	}
	elseif($country == 'us') {
		switch($orderType) {
			case 'RM': $type = '71000-704-71'; break;
			case 'AD': $type = '80043-700-00'; break;
			case 'SC': $type = '80080-704-70'; break;
			case 'CW': $type = '80060-704-70'; break;
			case 'RS': $type = '80130-704-00'; break;
			case 'PR': $type = '80060-704-70'; break;
			case 'SV': $type = '80120-800-00'; break;
			case NULL: $type = NULL; break;
		}
	}
	
	if($country == 'de' || $country == 'uk') {
		$locncode = ($country == 'de') ? 'DE-SV' : 'UK-TW ECOM';
	} elseif($country == 'us') {
		$locncode = ($order_wrapp->field_type_stock->value() == 1) ? 'US-DSV CA' : 'US-DSV NJ';
	}
	
	//creating eConnect XML
	$eConnect = new SimpleXMLElement('<eConnect xmlns:dt="urn:schemas-microsoft-com:datatypes"></eConnect>');
	$IVInventoryTransactionType = $eConnect->addChild('IVInventoryTransactionType');
	
	//creating taSopLineIvcInsert_Items for line products
	$Items = $IVInventoryTransactionType->addChild('taIVTransactionLineInsert_Items');
	$HeaderInsert = $IVInventoryTransactionType->addChild('taIVTransactionHeaderInsert');
	
	//Line Item level data
	foreach($order_wrapp->commerce_line_items as $line) {
		if($line->type->value() == 'product') {
			if($country == 'de' || $country == 'uk') {
				/* soap XML call parameters */
				switch($country) {
					case 'uk': $doctype = 'vwth_EC_Item_Comp_UK'; break;
					case 'de': $doctype = 'vwth_EC_Item_Comp_DE'; break;
					default: $doctype = 'vwth_EC_Item_Comp_UK'; break;
				}
				$soap_params = [
					'MessageID'	=> 'Item',
					'DOCTYPE' => $doctype,
					'WHERECLAUSE' => "ITEMNMBR = '" . $line->commerce_product->sku->value() . "'",
				];

				/* converting $eConnect object to XML format || using soap_xml function */
				$sDoc = soap_xml($soap_params);

				/* call soap_call function from global.inc */
				$data = soap_trx_call($sDoc, $server, $order_wrapp);
				
				$components = [];
				if(!empty($data['eConnect'][0])) { foreach($data['eConnect'] as $component_raw) { $components[] = $component_raw[$soap_params['DOCTYPE']]; } }
				else { $components[] = $data['eConnect'][$soap_params['DOCTYPE']]; }
				
				foreach($components as &$component) {
					$lineInsert = $Items->addChild('taIVTransactionLineInsert');
					$lineInsert->addChild('IVDOCNBR', $order_wrapp->order_number->value());
					$lineInsert->addChild('IVDOCTYP', '1');
					$lineInsert->addChild('ITEMNMBR', $component['CMPTITNM']);
					$lineInsert->addChild('TRXQTY', intval($line->quantity->value()) * intval($component['CMPITQTY']) * -1);
					$lineInsert->addChild('TRXLOCTN', $locncode);
					$lineInsert->addChild('InventoryAccountOffSet', $type);
				}
				
			} elseif($country == 'us') {
				$lineInsert = $Items->addChild('taIVTransactionLineInsert');
				$lineInsert->addChild('IVDOCNBR', $order_wrapp->order_number->value());
				$lineInsert->addChild('IVDOCTYP', '1');
				$lineInsert->addChild('ITEMNMBR', $line->commerce_product->sku->value());
				$lineInsert->addChild('TRXQTY', round($line->quantity->value()) * -1);
				$lineInsert->addChild('TRXLOCTN', $locncode);
				$lineInsert->addChild('InventoryAccountOffSet', $type);
			}
		}
	}
	
	//Adding Header level data
	$HeaderInsert->addChild('BACHNUMB', strtoupper($country) . date('dmY', $order_wrapp->field_invoice_date->value()));
	$HeaderInsert->addChild('IVDOCNBR', $order_wrapp->order_number->value());
	$HeaderInsert->addChild('IVDOCTYP', '1');
	$HeaderInsert->addChild('DOCDATE', date('Y-m-d', $order_wrapp->field_invoice_date->value()));
	$HeaderInsert->addChild('TRXQTYTL', count($Items->children()));
	
	$params = array(
        'sConn' => 'Data Source=Audio25;Initial Catalog=' . $server .';Persist Security Info=False;Integrated Security=SSPI;',
        'sXML' => $eConnect->asXml(),
    );
	
	try {
        $client = new SoapClient('http://econnect.cambridgeaudio.com/msgp2013adapter/msgp2013adapter.asmx?wsdl');
        $resp = $client->CreateEntity($params);
    } catch (Exception $e) {
		$int_trx = dynamics_failure($country, $order, $e, $eConnect->asXml());
    }
	
	$file_uri = 'public://trx_xml/TRX_' . $order_wrapp->order_number->value() . '_' . date('YmdHis'). '.xml';
	file_save_data($eConnect->asXml(), $file_uri, FILE_EXISTS_RENAME);
	
	/* ///// PERFORM DOUBLE TEST CHECK ///// */
	
	$soap_params_test = [
		'MessageID'	=> 'Item',
		'DOCTYPE' =>'vwth_InventoryTransactions',
		'WHERECLAUSE' => "IVDOCNBR = '" . $order_wrapp->order_number->value() . "'",
	];

	/* converting $eConnect object to XML format || using soap_xml function */
	$test_sDoc = soap_xml($soap_params_test);

	/* call soap_call function from global.inc */
	$data_test = soap_trx_call($test_sDoc, $server, $order_wrapp);
	
	$dynamics_data = [];
	if(!empty($data_test['eConnect'][0])) {
		foreach($data_test['eConnect'] as $test_component_raw) {
			$dynamics_data[] = $test_component_raw[$soap_params_test['DOCTYPE']];
		}
	}
	else {
		$dynamics_data[] = $data_test['eConnect'][$soap_params_test['DOCTYPE']];
	}
	
	$xmlCorrect = TRUE;
	foreach($Items as $item) {
		foreach($dynamics_data as $d_data) {
			if(in_array((string)$item->ITEMNMBR, $d_data)) {
				if(floatval($d_data['TRXQTY']) != (float)$item->TRXQTY) {
					$xmlCorrect = FALSE;
					$message = 'NUMBERS ARE WRONG // POV TEST LINE';
				}
			} else {
				$xmlCorrect = FALSE;
				$message = 'ITEMS ARE WRONG // POV TEST LINE';
			}
		}
	}
	
	if($xmlCorrect == FALSE) {
		/* Change this to your own default 'from' email address. */
		$from = variable_get('site_name', 'Default site name') . ' ' . strtoupper($country) . '<webadmin@cambridgeaudio.com>';
		$to = 'tim.huelin@cambridgeaudio.com';

		$body  = 'Hi,<br><br>';
		$body .= $message . 'Error occured while checking TRX in dynamics for order <b>' . $order_wrapp->order_number->value() . '</b>:<br><br>';
		$body .= 'Information retrieved from Dynamics:<pre>' . print_r($dynamics_data, true) . '</pre>';
		$body .= 'XML sent:<pre>' . print_r($eConnect, true) . '</pre>';
		$body .= variable_get('site_name', 'Default site name') . ' ' . strtoupper($country);

		$cc = 'tim.huelin@cambridgeaudio.com, povilas.uogintas@cambridgeaudio.com';

		$params = [
			'context' => [
				'subject' => 'Error occured while checking TRX in dynamics for order ' . $order_wrapp->order_number->value(),
				'body' => $body,
			],
			'cc' => $cc,
			'bcc' => NULL,
			'reply-to' => $from,
			'plaintext' => '',
		];

		drupal_mail('mimemail', 'trx_component_check_error', $to, NULL, $params, $from);
		
		commerce_order_status_update($order_wrapp->value(), 'dynamics_fail');
	}
	
	/* ///// PERFORM DOUBLE TEST CHECK END ///// */

	return [
        'soap_error' => !empty($int_trx['error']) ? $int_trx['error'] : NULL,
		'xml' => !empty($int_trx['order_xml']) ? $int_trx['order_xml'] : NULL,
    ];
}

function b2b_de_send_invoice($dynamics_server, $order_id) {

	$order = commerce_order_load($order_id);
	$dealer = user_load($order->uid);
	
	//$variables
	$CUSTNMBR = $dealer->name;
	$LOCNCODE = 'DE-SV';
	$TAXSCHID = 'DE-19%';
			
	//creating eConnect XML
	$eConnect = new SimpleXMLElement('<eConnect xmlns:dt="urn:schemas-microsoft-com:datatypes"></eConnect>');
	$SOPTransactionType = $eConnect->addChild('SOPTransactionType');
	
	//creating taSopLineIvcInsert_Items for line products
	$Items = $SOPTransactionType->addChild('taSopLineIvcInsert_Items');
	$ItemsTax = $SOPTransactionType->addChild('taSopLineIvcTaxInsert_Items');
	$HeaderInsert = $SOPTransactionType->addChild('taSopHdrIvcInsert');
	
	//getting Order Total prices
	$orderPrices = return_prices($order->commerce_order_total['und'][0]['data']['components']);
		
	//Adding Header level data
	$HeaderInsert->addChild('SOPTYPE', '3');
	$HeaderInsert->addChild('DOCID', 'INVOICE');
	$HeaderInsert->addChild('SOPNUMBE', $order->order_number);
	$HeaderInsert->addChild('LOCNCODE', $LOCNCODE);
	$HeaderInsert->addChild('DOCDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
	$HeaderInsert->addChild('CUSTNMBR', $CUSTNMBR);
	$HeaderInsert->addChild('CSTPONBR', (!empty($order->field_custom_order_reference)) ? $order->field_custom_order_reference['und'][0]['value'] : NULL);
	$HeaderInsert->addChild('USER2ENT', 'sa');
	$HeaderInsert->addChild('BACHNUMB', 'DEB2B');
	$HeaderInsert->addChild('PRBTADCD', 'MAIN');
	$HeaderInsert->addChild('MSCTXAMT', '0');
	$HeaderInsert->addChild('USINGHEADERLEVELTAXES', '0');
	$HeaderInsert->addChild('CREATETAXES', '0');
	$HeaderInsert->addChild('DEFTAXSCHDS', '0');
	$HeaderInsert->addChild('FREIGTBLE', '1');
	$HeaderInsert->addChild('MISCTBLE', '0');
	$HeaderInsert->addChild('SUBTOTAL', number_format($orderPrices['base_price']/100, 2, '.', ''));
	$HeaderInsert->addChild('DOCAMNT', number_format($order->commerce_order_total['und'][0]['amount']/100, 2, '.', ''));
	$HeaderInsert->addChild('PYMTRCVD');
	$HeaderInsert->addChild('TAXAMNT', number_format($orderPrices['tax']/100, 2, '.', ''));
	$HeaderInsert->addChild('TAXSCHID', $TAXSCHID);
	$HeaderInsert->addChild('CURNCYID', $order->commerce_order_total['und'][0]['currency_code']);
	
	//Line Item level data
	foreach($order->commerce_line_items['und'] as &$line) {
		$line_item = commerce_line_item_load($line['line_item_id']);
		if($line_item->type == 'product') {
			$product = commerce_product_load($line_item->commerce_product['und'][0]['product_id']);
			
			$unitPrices = return_prices($line_item->commerce_unit_price['und'][0]['data']['components']);
			$totalUnitPrices = return_prices($line_item->commerce_total['und'][0]['data']['components']);
			
			// taSopLineIvcInsert
			$lineInsert = $Items->addChild('taSopLineIvcInsert');
			$lineInsert->addChild('SOPTYPE', '3');
			$lineInsert->addChild('SOPNUMBE', $order->order_number);
			$lineInsert->addChild('CUSTNMBR', $CUSTNMBR);
			$lineInsert->addChild('DOCDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
			$lineInsert->addChild('LOCNCODE', $LOCNCODE);
			$lineInsert->addChild('ITEMNMBR', $product->sku);
			$lineInsert->addChild('QUANTITY', round($line_item->quantity));
			$lineInsert->addChild('UNITPRCE', number_format($unitPrices['base_price']/100, 2, '.', ''));
			$lineInsert->addChild('XTNDPRCE', number_format($totalUnitPrices['base_price']/100, 2, '.', ''));
			$lineInsert->addChild('TAXAMNT', !empty($totalUnitPrices['tax']) ?  number_format($totalUnitPrices['tax']/100, 2, '.', '') : NULL);
			$lineInsert->addChild('LNITMSEQ', $line_item->line_item_id);
			$lineInsert->addChild('DOCID', 'INVOICE');
			$lineInsert->addChild('CURNCYID', $order->commerce_order_total['und'][0]['currency_code']);

			// taSopLineIvcTaxInsert
			$lineTaxInsert = $ItemsTax->addChild('taSopLineIvcTaxInsert');
			$lineTaxInsert->addChild('SOPTYPE', '3');
			$lineTaxInsert->addChild('TAXTYPE', '0');
			$lineTaxInsert->addChild('SOPNUMBE', $order->order_number);
			$lineTaxInsert->addChild('CUSTNMBR', $CUSTNMBR);
			$lineTaxInsert->addChild('LNITMSEQ', $line_item->line_item_id);
			$lineTaxInsert->addChild('SALESAMT', number_format($totalUnitPrices['base_price']/100, 2, '.', ''));
			$lineTaxInsert->addChild('FRTTXAMT', '0');
			$lineTaxInsert->addChild('MSCTXAMT', '0');
			$lineTaxInsert->addChild('FREIGHT', '0');
			$lineTaxInsert->addChild('MISCAMNT', '0');
			$lineTaxInsert->addChild('TAXDTLID', $TAXSCHID);
			$lineTaxInsert->addChild('STAXAMNT', !empty($totalUnitPrices['tax']) ?  number_format($totalUnitPrices['tax']/100, 2, '.', '') : NULL);
		}
	}
	
	$server = ($dynamics_server == 'live') ? 'APPLC' : 'TRG01';
	
	$params = array(
        'sConn' => 'Data Source=Audio25;Initial Catalog=' . $server .';Persist Security Info=False;Integrated Security=SSPI;',
        'sXML' => $eConnect->asXml(),
    );
	
	try {
        $client = new SoapClient('http://econnect.cambridgeaudio.com/msgp2013adapter/msgp2013adapter.asmx?wsdl');
        $resp = $client->CreateEntity($params);
		
    } catch (Exception $e) {
		$invoice = dynamics_failure('b2b', $order, $e, $eConnect->asXml());
    }
	
	return array(
        'soap_error' => !empty($invoice['error']) ? $invoice['error'] : NULL,
		'xml' => !empty($invoice['order_xml']) ? $invoice['order_xml'] : NULL,
    );
}

function b2c_send_invoice($dynamics_server, $country, $order_id) {

	$order = commerce_order_load($order_id);
	$user = user_load($order->uid);
	
	$shipment = commerce_customer_profile_load($order->commerce_customer_shipping['und'][0]['profile_id']);
	$cstponbr = $shipment->commerce_customer_address['und'][0]['name_line'];
	
	//$CUSTNMBR
	if($country == 'de') { $CUSTNMBR = (in_array('partner', $user->roles)) ? 'EC004' : 'EC003'; }
	elseif($country == 'uk') { $CUSTNMBR = (in_array('partner', $user->roles)) ? 'EC002' : 'EC001'; }
	elseif($country == 'us') { $CUSTNMBR = (in_array('partner', $user->roles)) ? 'EC102' : 'EC101';	}
	if(in_array('colleague', $user->roles)) { $CUSTNMBR = str_replace(' ', '', $user->field_colleague_id['und'][0]['value']);	}
	
	$ebayOrder = FALSE;
	if(!empty($order->field_ebay_order) && $order->field_ebay_order['und'][0]['value'] == '1') {
		$CUSTNMBR = ($country == 'de') ? 'EB003' : 'EB002';
		$ebayOrder = TRUE;
	}
	
	//$langcode
	if($country == 'de') {
		$locncode = 'DE-SV';
	} elseif(($country == 'uk')) {
		if($ebayOrder == TRUE) {
			$locncode = 'UK-TW';
		} else {
			$locncode = 'UK-TW ECOM';
		}
	} elseif(($country == 'us')) {
		$first_item = commerce_line_item_load($order->commerce_line_items['und'][0]['line_item_id']);
		$locncode = ($first_item->field_type_stock['und'][0]['value'] == 1) ? 'US-DSV CA' : 'US-DSV NJ';
	}
	
	$taxRate = ($country == 'de') ? 1.19 : 1.20;
		
	//creating eConnect XML
	$eConnect = new SimpleXMLElement('<eConnect xmlns:dt="urn:schemas-microsoft-com:datatypes"></eConnect>');
	$SOPTransactionType = $eConnect->addChild('SOPTransactionType');
	
	//creating taSopLineIvcInsert_Items for line products
	$Items = $SOPTransactionType->addChild('taSopLineIvcInsert_Items');
	$ItemsTax = $SOPTransactionType->addChild('taSopLineIvcTaxInsert_Items');
	$PaymentInsert = $SOPTransactionType->addChild('taCreateSopPaymentInsertRecord_Items')->addChild('taCreateSopPaymentInsertRecord');
	$HeaderInsert = $SOPTransactionType->addChild('taSopHdrIvcInsert');
	
	//adding Payment information
	
	
	if($ebayOrder == TRUE) {
		$payAmmount = $order->commerce_order_total['und'][0]['amount'];
		$payCreated = $order->created;
		switch($country) {
			case 'uk': $chekbid = 'PAYPAL'; break;
			case 'de': $chekbid = 'PAYPAL EURO'; break;
			default: $chekbid = 'PAYPAL'; break;
		}
	} else {
		$payment = commerce_payment_transaction_load_multiple(array(), array('order_id' =>  $order->order_id));
		foreach($payment as &$pay) {
			if($pay->status == 'success') {
				if($pay->payment_method =='braintree_dropin') {
					foreach($pay->payload as &$payload) {
						if($pay->message == 'Processor response: Approved<br />Credit card type: <br />Credit card last 4: ') {
							switch($country) {
								case 'uk': $chekbid = 'PAYPAL'; break;
								case 'de': $chekbid = 'PAYPAL EURO'; break;
								case 'us': $chekbid = 'PAYPAL USD'; break;
								default: $chekbid = 'PAYPAL'; break;
							}
						} else {
							switch($country) {
								case 'uk': $chekbid = 'BRAINTREE GBP'; break;
								case 'de': $chekbid = 'BRAINTREE EURO'; break;
								case 'us': $chekbid = 'BRAINTREE USD'; break;
								default: $chekbid = 'BRAINTREE GBP'; break;
							}
						}
						$payAmmount = $pay->amount;
						$payCreated = ($country == 'de') ? $order->field_invoice_date['und'][0]['value'] : $pay->created;
					}
				}
			}
		}
	}
	
	$PaymentInsert->addChild('SOPTYPE', '3');
	$PaymentInsert->addChild('SOPNUMBE', $order->order_number);
	$PaymentInsert->addChild('CUSTNMBR', $CUSTNMBR);
	$PaymentInsert->addChild('DOCDATE', date('Y-m-d', $payCreated));
	$PaymentInsert->addChild('DOCAMNT', number_format($payAmmount/100, 2, '.', ''));
	$PaymentInsert->addChild('CHEKBKID', $chekbid);
	$PaymentInsert->addChild('PYMTTYPE', '4');
	
	//getting Order Total prices
	$orderPrices = return_prices($order->commerce_order_total['und'][0]['data']['components']);
	
	//UK & DE calculations...
	if($country == 'de' || $country == 'uk') {
		//Discount Calculations
		$orderDiscount = !empty($orderPrices['discount']) ? $orderPrices['discount']/$taxRate : NULL;

		//Subtotal Calculations
		$subtotal = 0;
		foreach($order->commerce_line_items['und'] as &$line) {
			$line_item = commerce_line_item_load($line['line_item_id']);
			if($line_item->type == 'product') {
				$subtotal += $line_item->field_no_vat_price['und'][0]['amount'] * $line_item->quantity;
			}
		}

		//Discount Calculations
		$orderDiscount = (!empty($orderPrices['discount']) || $orderPrices['discount'] != 0) ? $orderPrices['discount']/$taxRate : NULL;

		//TaxCalculations
		$orderTax = $subtotal + $orderDiscount - $order->commerce_order_total['und'][0]['amount'];

		//TAX id
		$taxid = ($country == 'de') ? 'DE-19%' : 'S-20%';
	}
	
	//Adding Header level data
	$HeaderInsert->addChild('SOPTYPE', '3');
	$HeaderInsert->addChild('DOCID', ($ebayOrder == TRUE) ? 'S' . strtoupper($country) . 'EB' : 'INVOICE');
	$HeaderInsert->addChild('SOPNUMBE', $order->order_number);
	$HeaderInsert->addChild('LOCNCODE', $locncode);
	$HeaderInsert->addChild('DOCDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
	$HeaderInsert->addChild('CUSTNMBR', $CUSTNMBR);
	$HeaderInsert->addChild('CSTPONBR', $cstponbr);
	$HeaderInsert->addChild('USER2ENT', 'sa');
	
	if($ebayOrder == TRUE) {
		$BACHNUMB = 'EBAY';
	} else {
		switch($country) {
			case 'uk': $BACHNUMB = 'B2CUK'; break;
			case 'de': $BACHNUMB = 'B2CDE'; break;
			case 'us': $BACHNUMB = 'B2CUS'; break;
			default: $BACHNUMB = 'B2CUK'; break;
		}
	}
	
	$HeaderInsert->addChild('BACHNUMB', $BACHNUMB);
	$HeaderInsert->addChild('PRBTADCD', 'MAIN');
	$HeaderInsert->addChild('FRTTXAMT', '0.00');
	$HeaderInsert->addChild('MSCTXAMT', '0.00');
	$HeaderInsert->addChild('USINGHEADERLEVELTAXES', '0');
	$HeaderInsert->addChild('CREATETAXES', '0');
	$HeaderInsert->addChild('DEFTAXSCHDS', '0');
	$HeaderInsert->addChild('CURNCYID', $order->commerce_order_total['und'][0]['currency_code']);
	$HeaderInsert->addChild('FREIGTBLE', '1');
	$HeaderInsert->addChild('MISCTBLE', '0');
	
	if($country == 'de' || $country == 'uk') {
		$HeaderInsert->addChild('TRDISAMT', number_format($orderDiscount/-100, 2, '.', ''));
		$HeaderInsert->addChild('SUBTOTAL', number_format($subtotal/100, 2, '.', ''));
		$HeaderInsert->addChild('DOCAMNT', number_format($order->commerce_order_total['und'][0]['amount']/100, 2, '.', ''));
		$HeaderInsert->addChild('PYMTRCVD', number_format($payAmmount/100, 2, '.', ''));
		$HeaderInsert->addChild('TAXAMNT', number_format($orderTax/-100, 2, '.', ''));
		$HeaderInsert->addChild('TAXSCHID', !empty($orderTax) ? $taxid : 'S-ZERO-ROW');
	} elseif($country == 'us') {
		$HeaderInsert->addChild('TRDISAMT', (!empty($orderPrices['discount']) || $orderPrices['discount'] != 0) ? number_format($orderPrices['discount']/-100, 2, '.', '') : NULL);
		$HeaderInsert->addChild('SUBTOTAL', number_format($orderPrices['base_price']/100, 2, '.', ''));
		$HeaderInsert->addChild('DOCAMNT', number_format($order->commerce_order_total['und'][0]['amount']/100, 2, '.', ''));
		$HeaderInsert->addChild('PYMTRCVD', number_format($payAmmount/100, 2, '.', ''));
		$HeaderInsert->addChild('TAXAMNT', !empty($orderPrices['tax']) ? number_format($orderPrices['tax']/100, 2, '.', '') : NULL);
		$HeaderInsert->addChild('TAXSCHID', 'SALES - FL - 0%');
	}
	
	
	//Line Item level data
	foreach($order->commerce_line_items['und'] as &$line) {
		$line_item = commerce_line_item_load($line['line_item_id']);
		if($line_item->type == 'product') {
			$product = commerce_product_load($line_item->commerce_product['und'][0]['product_id']);
			
			if($country == 'de' || $country == 'uk') {
				$price_noVat = $line_item->field_no_vat_price['und'][0]['amount'];
				$extended_price_noVat = $price_noVat * $line_item->quantity;
				//taxCalculations
				if(empty($orderDiscount)) {
					$tax = $line_item->commerce_total['und'][0]['amount'] - $extended_price_noVat;
					$salesAmmount = $extended_price_noVat;
				} else {
					$fraction = $extended_price_noVat / round($subtotal);
					$disc_ammount = round($orderDiscount * $fraction);
					$salesAmmount = $extended_price_noVat + $disc_ammount;
					$tax = $line_item->commerce_total['und'][0]['amount'] - $salesAmmount;
				}
			} elseif($country == 'us') {
				$unitPrices = return_prices($line_item->commerce_unit_price['und'][0]['data']['components']);
				$totalPrices = return_prices($line_item->commerce_total['und'][0]['data']['components']);
			}
			
			// taSopLineIvcInsert
			$lineInsert = $Items->addChild('taSopLineIvcInsert');
			$lineInsert->addChild('SOPTYPE', '3');
			$lineInsert->addChild('SOPNUMBE', $order->order_number);
			$lineInsert->addChild('CUSTNMBR', $CUSTNMBR);
			$lineInsert->addChild('DOCDATE', date('Y-m-d', $order->field_invoice_date['und'][0]['value']));
			$lineInsert->addChild('LOCNCODE', $locncode);
			$lineInsert->addChild('ITEMNMBR', $product->sku);
			$lineInsert->addChild('QUANTITY', round($line_item->quantity));
			
			if($country == 'de' || $country == 'uk') {
				$lineInsert->addChild('UNITPRCE', number_format($price_noVat/100, 2, '.', ''));
				$lineInsert->addChild('XTNDPRCE', number_format($extended_price_noVat/100, 2, '.', ''));
				$lineInsert->addChild('TAXAMNT', number_format($tax/100, 2, '.', ''));
			} elseif($country == 'us') {
				$lineInsert->addChild('UNITPRCE', number_format($unitPrices['base_price']/100, 2, '.', ''));
				$lineInsert->addChild('XTNDPRCE', number_format($totalPrices['base_price']/100, 2, '.', ''));
				$lineInsert->addChild('TAXAMNT', !empty($totalPrices['tax']) ?  number_format($totalPrices['tax']/100, 2, '.', '') : NULL);
			}
			
			$lineInsert->addChild('LNITMSEQ', $line_item->line_item_id);
			$lineInsert->addChild('DOCID', ($ebayOrder == TRUE) ? 'SUKEB' : 'INVOICE');
			$lineInsert->addChild('CURNCYID', $order->commerce_order_total['und'][0]['currency_code']);

			// taSopLineIvcTaxInsert
			$lineTaxInsert = $ItemsTax->addChild('taSopLineIvcTaxInsert');
			$lineTaxInsert->addChild('SOPTYPE', '3');
			$lineTaxInsert->addChild('TAXTYPE', '0');
			$lineTaxInsert->addChild('SOPNUMBE', $order->order_number);
			$lineTaxInsert->addChild('CUSTNMBR', $CUSTNMBR);
			$lineTaxInsert->addChild('LNITMSEQ', $line_item->line_item_id);
			
			if($country == 'de' || $country == 'uk') {
				$lineTaxInsert->addChild('SALESAMT', number_format($salesAmmount/100, 2, '.', ''));
			} elseif($country == 'us') {
				$salesAmmount = !empty($totalPrices['discount'] || $totalPrices['discount'] != 0) ? $totalPrices['base_price'] + $totalPrices['discount'] : $totalPrices['base_price'];
				$lineTaxInsert->addChild('SALESAMT', number_format($salesAmmount/100, 2, '.', ''));
			}
			
			$lineTaxInsert->addChild('FRTTXAMT', '0');
			$lineTaxInsert->addChild('MSCTXAMT', '0');
			$lineTaxInsert->addChild('FREIGHT', '0');
			$lineTaxInsert->addChild('MISCAMNT', '0');
			if($country == 'de' || $country == 'uk') {
				$lineTaxInsert->addChild('TAXDTLID', !empty($orderTax) ? $taxid : 'S-ZERO-ROW');
				$lineTaxInsert->addChild('STAXAMNT', number_format($tax/100, 2, '.', ''));
			} elseif($country == 'us') {
				$lineTaxInsert->addChild('TAXDTLID', 'SALES - FL - 0%');
				$lineTaxInsert->addChild('STAXAMNT', !empty($totalPrices['tax']) ?  number_format($totalPrices['tax']/100, 2, '.', '') : NULL);
			}
		}
	}
	
	if($country == 'de' || $country == 'uk') {
		$server = ($dynamics_server == 'live') ? 'APPLC' : 'TRG01';
	} elseif($country == 'us') {
		$server = ($dynamics_server == 'live') ? 'APLLC' : 'TRG01';
	}
	
	$params = array(
        'sConn' => 'Data Source=Audio25;Initial Catalog=' . $server .';Persist Security Info=False;Integrated Security=SSPI;',
        'sXML' => $eConnect->asXml(),
    );
	
	try {
        $client = new SoapClient('http://econnect.cambridgeaudio.com/msgp2013adapter/msgp2013adapter.asmx?wsdl');
        $resp = $client->CreateEntity($params);
		
    } catch (Exception $e) {
		$invoice = dynamics_failure($country, $order, $e, $eConnect->asXml());
    }
	
	return array(
        'soap_error' => !empty($invoice['error']) ? $invoice['error'] : NULL,
		'xml' => !empty($invoice['order_xml']) ? $invoice['order_xml'] : NULL,
    );
}

function set_custom_order_counter($country, $type) {
	if(empty(variable_get($type . '_c_order_number', NULL))) {
		variable_set($type . '_c_order_number', 1);
		return [
			'order_number' => $type . strtoupper($country) . sprintf('%09d', 1),
		];
	} else {
		$number = variable_get($type . '_c_order_number', NULL);
		$number++;
		variable_set($type . '_c_order_number', $number);
		
		return [
			'order_number' => $type . strtoupper($country) . sprintf('%09d', $number),
		];
	}
}

?>