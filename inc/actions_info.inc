<?php

function cambridge_invoicing_rules_action_info() {
	$actions = array();
	
	$actions['custom_orders_counter'] = array(
		'label' => t('Custom Order counter fetch'),
		'group' => t('Cambridge Actions'),
		'parameter' => array(
			'ordertype' => array(
				'type' => 'text',
				'label' => t('Order Type'),
			),
			'site' => array(
				'type' => 'text',
				'label' => t('Site Name'),
			),
		),
		'provides' => array(
			'prefix' => array(
				'type' => 'text',
				'label' => t('Prefix for order number'),
			),
			'counter_id' => array(
				'type' => 'integer',
				'label' => t('Counter ID'),
			),
		),
	);
	
	$actions['hk_send_caa_invoice'] = array(
		'label' => t('HK send CAA invoice'),
		'group' => t('CA Dynamics HK'),
		'parameter' => array(
			'dynamics_server' => array(
				'type' => 'text',
				'label' => t('Live or Test'),
				'description' => t('Select to use TEST or LIVE server'),
				'options list' => 'dynamics_server_options',
				'restriction' => 'input',
			),
			'shop' => array(
				'type' => 'text',
				'label' => t('B2B or B2C'),
				'description' => t('Select to use B2B or B2C system'),
				'options list' => 'shop_options',
				'restriction' => 'input',
			),
			'order_id' => array(
				'type' => 'integer',
				'label' => t('Commerce Order ID'),
				'description' => t('Enter the ID of commerce order'),
			),
		),
		'provides' => array(
			'caa_soap_error' => array(
				'type' => 'text',
				'label' => t('CAA Erorr Code'),
			),
			'caa_xml' => array(
				'type' => 'text',
				'label' => t('CAA XML document location'),
			),
		),
	);
	
	$actions['hk_send_applc_invoice'] = array(
		'label' => t('HK send APPLC invoice'),
		'group' => t('CA Dynamics HK'),
		'parameter' => array(
			'dynamics_server' => array(
				'type' => 'text',
				'label' => t('Live or Test'),
				'description' => t('Select to use TEST or LIVE server'),
				'options list' => 'dynamics_server_options',
				'restriction' => 'input',
			),
			'shop' => array(
				'type' => 'text',
				'label' => t('B2B or B2C'),
				'description' => t('Select to use B2B or B2C system'),
				'options list' => 'shop_options',
				'restriction' => 'input',
			),
			'order_type' => array(
				'type' => 'text',
				'label' => t('Normal or Custom Order'),
				'options list' => 'order_type_options',
				'restriction' => 'input',
			),
			'order_id' => array(
				'type' => 'integer',
				'label' => t('Commerce Order ID'),
				'description' => t('Enter the ID of commerce order'),
			),
		),
		'provides' => array(
			'applc_soap_error' => array(
				'type' => 'text',
				'label' => t('APPLC Erorr Code'),
			),
			'applc_xml' => array(
				'type' => 'text',
				'label' => t('APPLC XML document location'),
			),
		),
	);
	
	$actions['hk_send_purchase_order'] = array(
		'label' => t('HK send Purchase Order'),
		'group' => t('CA Dynamics HK'),
		'parameter' => array(
			'dynamics_server' => array(
				'type' => 'text',
				'label' => t('Live or Test'),
				'description' => t('Select to use TEST or LIVE server'),
				'options list' => 'dynamics_server_options',
				'restriction' => 'input',
			),
			'shop' => array(
				'type' => 'text',
				'label' => t('B2B or B2C'),
				'description' => t('Select to use B2B or B2C system'),
				'options list' => 'shop_options',
				'restriction' => 'input',
			),
			'order_id' => array(
				'type' => 'integer',
				'label' => t('Commerce Order ID'),
				'description' => t('Enter the ID of commerce order'),
			),
		),
		'provides' => array(
			'purchase_soap_error' => array(
				'type' => 'text',
				'label' => t('Purchase Erorr Code'),
			),
			'purchase_xml' => array(
				'type' => 'text',
				'label' => t('Purchase XML document location'),
			),
		),
	);
	
	$actions['hk_send_inventory_trx'] = array(
		'label' => t('HK send Inventory TRX'),
		'group' => t('CA Dynamics HK'),
		'parameter' => array(
			'dynamics_server' => array(
				'type' => 'text',
				'label' => t('Live or Test'),
				'description' => t('Select to use TEST or LIVE server'),
				'options list' => 'dynamics_server_options',
				'restriction' => 'input',
			),
			'order_id' => array(
				'type' => 'integer',
				'label' => t('Commerce Order ID'),
				'description' => t('Enter the ID of commerce order'),
			),
		),
		'provides' => array(
			'int_trx_soap_error' => array(
				'type' => 'text',
				'label' => t('Inventory TRX Erorr Code'),
			),
			'int_trx_xml' => array(
				'type' => 'text',
				'label' => t('Inventory TRX XML document location'),
			),
		),
	);
	
	$actions['b2c_send_invoice'] = array(
		'label' => t('UK/DE send dynamics invoice'),
		'group' => t('CA Dynamics'),
		'parameter' => array(
			'dynamics_server' => array(
				'type' => 'text',
				'label' => t('Live or Test'),
				'description' => t('Select to use TEST or LIVE server'),
				'options list' => 'dynamics_server_options',
				'restriction' => 'input',
			),
			'country' => array(
				'type' => 'text',
				'label' => t('DE or UK'),
				'description' => t('Select to use UK or DE system'),
				'options list' => 'country_options',
				'restriction' => 'input',
			),
			'order_id' => array(
				'type' => 'integer',
				'label' => t('Commerce Order ID'),
				'description' => t('Enter the ID of commerce order'),
			),
		),
		'provides' => array(
			'soap_error' => array(
				'type' => 'text',
				'label' => t('Erorr Code'),
			),
			'xml' => array(
				'type' => 'text',
				'label' => t('XML document location'),
			),
		),
	);
	
	$actions['b2b_de_send_invoice'] = array(
		'label' => t('B2B DE send dynamics invoice'),
		'group' => t('CA Dynamics'),
		'parameter' => array(
			'dynamics_server' => array(
				'type' => 'text',
				'label' => t('Live or Test'),
				'description' => t('Select to use TEST or LIVE server'),
				'options list' => 'dynamics_server_options',
				'restriction' => 'input',
			),
			'order_id' => array(
				'type' => 'integer',
				'label' => t('Commerce Order ID'),
				'description' => t('Enter the ID of commerce order'),
			),
		),
		'provides' => array(
			'soap_error' => array(
				'type' => 'text',
				'label' => t('Erorr Code'),
			),
			'xml' => array(
				'type' => 'text',
				'label' => t('XML document location'),
			),
		),
	);
	
	$actions['send_inventory_trx'] = array(
		'label' => t('UK/DE send Inventory TRX'),
		'group' => t('CA Dynamics'),
		'parameter' => array(
			'dynamics_server' => array(
				'type' => 'text',
				'label' => t('Live or Test'),
				'description' => t('Select to use TEST or LIVE server'),
				'options list' => 'dynamics_server_options',
				'restriction' => 'input',
			),
			'country' => array(
				'type' => 'text',
				'label' => t('DE or UK'),
				'description' => t('Select to use UK or DE system'),
				'options list' => 'country_options',
				'restriction' => 'input',
			),
			'order_id' => array(
				'type' => 'integer',
				'label' => t('Commerce Order ID'),
				'description' => t('Enter the ID of commerce order'),
			),
		),
		'provides' => array(
			'soap_error' => array(
				'type' => 'text',
				'label' => t('Erorr Code'),
			),
			'xml' => array(
				'type' => 'text',
				'label' => t('XML document location'),
			),
		),
	);
	
	$actions['set_custom_order_counter'] = array(
		'label' => t('Set Custom Order Counter'),
		'group' => t('CA Dynamics'),
		'parameter' => array(
			'country' => array(
				'type' => 'text',
				'label' => t('Country'),
				'options list' => 'country_options',
				'restriction' => 'input',
			),
			'type' => array(
				'type' => 'text',
				'label' => t('Order Type'),
			),
		),
		'provides' => array(
			'order_number' => array(
				'type' => 'text',
				'label' => t('Number'),
			),
		),
	);
	
	return $actions;
}

function dynamics_server_options() {
	$dynamics_server = array();

	$dynamics_server['test'] = t('test');
	$dynamics_server['live'] = t('live');

	return $dynamics_server;
}

function shop_options() {
	$shop = array();

	$shop['b2b'] = t('B2B');
	$shop['b2c'] = t('B2C');

	return $shop;
}

function country_options() {
	$country = array(
		'uk' => 'UK',
		'de' => 'DE',
		'us' => 'US',
		'hk' => 'HK',
	);
	return $country;
}

function order_type_options() {
	$order_type = array();

	$order_type['custom'] = t('Custom');
	$order_type['normal'] = t('Normal');

	return $order_type;
}

?>